pipeline {
    options {
        ansiColor('xterm')
        buildDiscarder logRotator(artifactNumToKeepStr: '5', numToKeepStr: '20')
    }

    environment {
        // Dataverse component
        dataverse_module_name = "dvs-dataverse"
        dataverse_image_tag = "${docker_repo}/${dataverse_module_name}:${env.BRANCH_NAME}-${env.BUILD_NUMBER}"
    }

    agent any

    stages {
        stage('Check environment') {
            steps {
                echo "Currently Set Environment:"

                echo "dataverse_module_name = ${dataverse_module_name}"
                echo "dataverse_image_tag = ${dataverse_image_tag}"
            }
        }
        //Build stages
        stage("Build Dataverse Docker Image"){
            steps{
                echo "Building Docker image using Dockerfile with tag: ${dataverse_image_tag}"
                sh("docker build -t ${dataverse_image_tag} .")
            }
        }
        //Push stages
        stage('Push Dataverse Docker image'){
            steps{
                echo 'Tag and push Docker image'
                sh("gcloud auth configure-docker")
                sh("docker push ${dataverse_image_tag}")
                sh("gcloud container images add-tag ${dataverse_image_tag} ${docker_repo}/${dataverse_module_name}:${env.BRANCH_NAME}-latest")
            }
            when { branch 'master' }
        }
        //Deployment
        stage('Check Requirements and Deployments') {
            steps {
                dir('./infrastructure/gcp/') {
                    build job: 'cessda.dvs.deploy/master', parameters: [string(name: 'dataverse_image_tag', value: "${env.BRANCH_NAME}-${env.BUILD_NUMBER}"), string(name: 'module', value: 'dataverse'), string(name: 'cluster', value: 'development-cluster')], wait: false
                }
            }
            when { branch 'master' }
        }
    }
}
