[![Build Status](https://jenkins.cessda.eu/buildStatus/icon?job=cessda.dataverse.sshoc%2Fmaster)](https://jenkins.cessda.eu/job/cessda.dataverse.sshoc/job/master/)

#  Dataverse SSHOC

This repository contains all code needed to build the Dataverse Docker images.  
Separate repositories are provided for backend (Solr), testing and deployment.  
SSHOC Dataverse is the [dataverse-kubernetes](https://github.com/IQSS/dataverse-kubernetes) application deployed on CESSDA-cloud-infra.  
Please see [https://dataverse-k8s.readthedocs.io/en/latest/](dataverse-k8s.readthedocs.io) to install and run this application on your own infrastructure.


## Contributing

Please read [Contributing to CESSDA Open Source Software](https://bitbucket.org/cessda/cessda.guidelines.public/src/master/CONTRIBUTING.md)
for information on contribution to CESSDA software.


## Authors

You can find the list of all contributors [here](CONTRIBUTORS.md)

## License

This project is licensed under the Apache 2 License - see the [LICENSE](LICENSE.txt) file for details

## Acknowledgments

[Deploying, Running and Using Dataverse on Kubernetes](https://github.com/IQSS/dataverse-kubernetes)

